package com.esliceu.rfidpass.amarillo.gestordedatos.resources;

public enum DiasSemana {
    LUNES,
    MARTES,
    MIERCOLES,
    JUEVES,
    VIERNES,
    SABADO,
    DOMINGO
}
