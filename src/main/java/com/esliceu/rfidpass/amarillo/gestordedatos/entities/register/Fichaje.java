package com.esliceu.rfidpass.amarillo.gestordedatos.entities.register;

import com.esliceu.rfidpass.amarillo.gestordedatos.entities.persons.Usuario;
import com.esliceu.rfidpass.amarillo.gestordedatos.entities.tools.Lector;

import javax.persistence.*;

@Entity
@Table(name = "Fichaje")
public class Fichaje {

    @Id
    @Column(name = "Id", nullable = false)
    private Integer id;

    @Column(name = "Tipo", nullable = false)
    private String tipo;

    @Column(name = "Data", nullable = false)
    private String data;

    @ManyToOne(fetch = FetchType.LAZY)
    private Lector lector;

    @ManyToOne(fetch = FetchType.LAZY)
    private Usuario usuario;

    public Fichaje() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Lector getLector() {
        return lector;
    }

    public void setLector(Lector lector) {
        this.lector = lector;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
